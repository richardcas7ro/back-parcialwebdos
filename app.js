var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const cors = require('cors');

//const config = require('./config');


//Importa las rutas
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users.route');
var postsRouter = require('./routes/posts.route');
var followRouter = require('./routes/follow.route');

/*const config = {
    application: {
        cors: {
            server: [
                {
                    origin: "localhost:4200", //servidor que deseas que consuma o (*) en caso que sea acceso libre
                    credentials: true
                }
            ]
        }
}
}*/

//Importa la dbmanager
const dbmanager = require('./database/db.manager');


var app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
//app.use(cors(config.application.cors.server));
  app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
    next();
  });

//IMPORTANTE 
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/posts', postsRouter);
app.use('/follow', followRouter);
// se prueba la Conexion y si es exitosa se sincroniza y si no bota el error
dbmanager.sequelizeConexion.authenticate().then(
    () => {
        console.log("**************Se ha conectado a su base de datos mysqlWorkbench*******************");
        dbmanager.sequelizeConexion.sync().then(
            ()=>{
                console.log("Base de datos Sincronizada");
            }

        );
    }

).catch(
    error => {
        console.log("Error de conexion en la base de datos",error);
    }

)

module.exports = app;
