// por cada modelo se hace un controlador

const dbmanager = require('../database/db.manager');

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */

function crearusuario(req,res){
    // verificar que el req no este vacio
    if(!req.body){
        res.status(400).send(
            {

            message: "Request esta vacio"

             }
        );
        return;
        
    }
    // crear el objeto usuario
    const newUserObject ={
        username: req.body.username,
        password: req.body.password,
        creationdate: req.body.creationdate,
        autentication:req.body.autentication
    }
    // Insertando en la abse de datos el objeto
    dbmanager.User.create(newUserObject).then(
        data => {
            res.send(data);
        }

    ).catch(
        error => {
            console.log(error);
            res.status (500).send(
                {
                    message: " ERRRRRROOOOOOOOOORRRRRRRR" + error

                }

            );
        }    
        
        
    );

}

async function findallusers(req,res){
    try{const allusers = await dbmanager.User.findAll();

        res.send(
            {
                data: allusers
            }
        );
    }catch(error){
        console.log(error)
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO"
            }
        )
    }
}

async function findUserByUser(req,res){
    try{

        // se trae el parametro
       const { username } = req.params;

       const user = await dbmanager.User.findOne(
           {
               where: {
                   username: username
               }
           }
       );
        
        res.json(user);
        

    }catch(esrror){
        console.log(error,"Perdonameeeeee")
        res.status(500).send(
            {
                message:"ERRORR,LO SENTIMOS ESTAMOS TRABAJANDO PARA SOLUCIONARLO,"
            }
        )

    }
}

async function login(req,res){
    try{
        // se trae el parametro

       const user = await dbmanager.User.findOne(
           {
               where: {
                username: req.query.username,
                password: req.query.password
               }
           }
       );
     
        res.json(user);
       
    
    }catch(esrror){
        console.log(error,"userandpass not found")
        res.status(500).send(
            {
                message:"userandpass not found"
            }
        )

    }
}

async function DeleteByUsername (req,res) {
    const { username} = req.params;
    
   await dbmanager.User.destroy({
        where: {
            username : username
        }
     })
     res.send(
         {
             message: "Eliminado"
         }
     )
    }
exports.crearusuario = crearusuario;
exports.findallusers = findallusers;
exports.findUserByUser = findUserByUser;
exports.DeleteByUsername = DeleteByUsername;
exports.login = login;