const dbconfiguracion = require('./db.configuracion');
const sequelize = require('sequelize');


const sequelizeconexion = new sequelize(
    dbconfiguracion.DB,
    dbconfiguracion.USER,
    dbconfiguracion.PASSWORD,
    {
        host: dbconfiguracion.HOST,
        dialect: dbconfiguracion.dialect,
        operatorAliases: false,
        pool: {
            max: dbconfiguracion.pool.max,
            min: dbconfiguracion.pool.min,
            acquire: dbconfiguracion.pool.acquire,
            idle: dbconfiguracion.pool.idle


        }
    }
)

module.exports = sequelizeconexion;