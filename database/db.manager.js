const Sequelize = require('sequelize');

const sequelizeConexion = require('./db.conexion');


// Importar modelos

const UserModel = require('../models/user.model');
const PostModel = require('../models/post.model');
const Followmodel = require('../models/follow.model');


//Inicializar modelos

const User = UserModel(sequelizeConexion,Sequelize);
const Post = PostModel(sequelizeConexion,Sequelize);
const Follow = Followmodel(sequelizeConexion,Sequelize);

User.hasMany(Post,{ foreingkey:'idPost',sourcekey:'idUser'});
Post.belongsTo(User,{foreingkey:'idUser',sourcekey:'idPost'});
//Follow.hasMany(Follow,{foreingkey:'idFollow',sourcekey:'idUser'});
//Follow.belongsTo(User,{foreingkey:'idUser',sourcekey:'idFollow'})

const models = {
    User: User,
    Post: Post,
    Follow: Follow
}
const db ={

    ...models,
    sequelizeConexion
}

module.exports= db;
