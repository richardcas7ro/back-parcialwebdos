module.exports = (sequelize,Sequelize) => {
    const Follow = sequelize.define('Follow',
        {
            idFollow:{
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            username: {
                type: Sequelize.STRING,
                unique: false
                

            },
            userfollow: Sequelize.STRING
             },
             {
            tableName: "Follows"
        }


    );

    return Follow;

}
