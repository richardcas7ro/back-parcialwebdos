module.exports = (sequelize,Sequelize) => {
    const Post = sequelize.define('Post',
        {
            idPost:{
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            username: {
                type: Sequelize.STRING,
                unique: false
                

            },
            message: Sequelize.STRING,
             creationdate: Sequelize.DATE,
             },
             {
            tableName: "Posts"
        }


    );

    return Post;

}