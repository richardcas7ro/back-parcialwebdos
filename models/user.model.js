

// en esta carpeta Models creamos las tablas

module.exports = (sequelize,Sequelize) => {
    const User = sequelize.define('User',
        {
            idUser:{
                type: Sequelize.INTEGER,
                primaryKey: true,
                autoIncrement: true
            },
            username: {
                type: Sequelize.STRING,
                unique: true
                

            },
            password: {
                type: Sequelize.STRING,               

            }
            ,
            creationdate: Sequelize.DATE,
            autentication: {
                type: Sequelize.BOOLEAN
                  //defaultValue: false
            }
            




        },{
            tableName: "Users"
        }


    );

    return User;

}