var express = require('express');
var router = express.Router();
const Usercontroller = require('../controllers/user.controller');

router.get('/login/',Usercontroller.login );
/* Post users listing. */
router.post('/',Usercontroller.crearusuario );
// Se puede dejar la misma ruta pues  son diferentes metodos
// Get todos los usuarios
router.get('/',Usercontroller.findallusers );

//Get usuario por id
router.get('/:idUser',Usercontroller.findUserByUser);

router.delete('/:username',Usercontroller.DeleteByUsername );





module.exports = router;
